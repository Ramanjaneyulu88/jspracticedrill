const userDetailsArray = [
  {
    id: "600dc3b5d617e547a0e74cb9",
    name: "Mitchell Fitzgerald",
    about:
      "Proident voluptate veniam voluptate mollit reprehenderit anim officia et ea ex laboris nulla laboris. Nulla ut aliquip fugiat tempor veniam sint aliqua reprehenderit tempor Lorem commodo anim.",
    address: "48 Flatlands Avenue, Cutter, North Dakota",
    address: {
      streetAddress: "48 Flatlands Avenue",
      neighbour: "Cutter",
      city: "North Dakota",
    },
    company: "Scenty",
  },
  {
    id: "600dc3b5c4e60ba2ebf06569",
    name: "Howell Faulkner",
    about:
      "Mollit Lorem reprehenderit qui elit id aliqua. Deserunt ipsum ad cupidatat ullamco ut aliqua est do consectetur nostrud sit esse.",
    address: {
      streetAddress: "77 Hemlock Street",
      neighbour: "Hasty",
      city: "Florida",
    },
    company: "Fleetmix",
  },
];

// Problem 1
const getAllAddressUsingMap = (details) => {
  return details.map((eachAddress, index) => {
    return eachAddress["address"];
  });
};

// Problem 2
const getAllAddressesUsingReduce = (details) => {
  return details.reduce((acc, eachAddress) => {
    acc[eachAddress["id"]] = eachAddress["address"];
    return acc;
  }, {});
};

//Problem 3
const getAllUserDetails = (details) => {
  return details.reduce((acc, eachAddress) => {
    

    let {id, ...rest} = eachAddress
    acc[id] = rest;

    return acc;
  }, {});
};

//  Problem4

const filterOutPersonProblemFour = (userDetails) => {
  return userDetails.reduce((acc, eachAddress) => {
    if (eachAddress["address"]["city"] === "North Dakota") {
      let id = eachAddress["id"];
      acc[id] = {};
      acc[id]["information"] = [
        eachAddress["name"],
        eachAddress["address"]["city"],
        eachAddress["company"],
      ];
    }
    return acc;
  }, {});
};

// Problem 5



const filterOutPersonProblemFive = (details) => {
    
 return  details
    .reduce((newArray,eachAddress) => {
      if (eachAddress["address"]["city"] === "North Dakota") {
        newArray.push({id:eachAddress["id"],name:eachAddress["name"]})
        newArray.push(eachAddress["address"]["city"])
        newArray.push(eachAddress["company"])
      }
      return newArray
    },[])
   
};

// Problem6
const filterOutPersonProblemSix = (userDetails) => {
  let newArray = [];

  userDetails.forEach((eachAddress) => {
    if (eachAddress["address"]["city"] === "North Dakota") {
       
    
      newArray.push({ id: eachAddress["id"], name: eachAddress["name"] });
      newArray.push({ city: eachAddress["address"]["city"] });
      newArray.push({ company: eachAddress.company });
    } });

  return newArray;
};

// Problem 7
const transformUserDetails = (userDetails) => {
  return userDetails.map((eachAddress) => {
    return [
      { id: eachAddress["id"], name: eachAddress["name"] },
      { city: eachAddress["address"]["city"] },
      { company: eachAddress["company"] },
    ];
  });
};

module.exports = {
  getAllAddressUsingMap,
  getAllAddressesUsingReduce,
  getAllUserDetails,
  filterOutPersonProblemFour,
  filterOutPersonProblemFive,
  filterOutPersonProblemSix,
  transformUserDetails,
};
